package INICIO;

import java.util.Scanner;

public class ModuloClienteInterno {
    static String[] sucursales = {"Arequipa", "Arequipa", "Ica", "Ica","Ica", "Cerro de Pasco", "Cerro de Paso", "Lima","Lima"};
    static long[] RUC={2060500212,2006002231,323495734,343449272,1233442421,1349373443,1233434534,344550854,234023433};
    static String[] razonSocial={"Autral Group SA","San Martin Contratistas SA","Profuturo AFP","Agricola de la Chira",
            "Stracon","Unicon", "Volcan","AESA", "Pesquera Caral"};
    static String[] personaDeContacto={"Martin Rojas", "Juan Mendoza","Claudia Guevara","Marco Rojas", "Jhon Mejia","Josselin Burga",
            "Carolina Delgado", "Kevin Yaipen","Cesar Tenicela"};
    static int[] satisfaGeneral={9,8,7,3,5,8,9,9,9};
    static int[] intenRecompra = {9,9,6,2,8,9,9,6,5};
    static int[] intenRecomendacion = {9,6,4,7,7,7,9,9,7};
    static String[] categoriaLealtad={"Leal","Vulnerable","Riesgo","Riesgo","Riesgo","Vulnerable","Leal","Vulnerable","Riesgo"};
    static String[] sugerencia={"Todo bien","Mejorar el tiempo de entrega","Mejorar la actitud del vendedor","Incrementar el nivel de stock",
            "Incrementar el crédito a sus clientes recurrentes","Reducir el tiempo de espera en almacén","Regalar merchadising",
            "Mejorar el empaque de las entregas","Mucha demora en la espera de atención"};

    public static void main(String[] args) {


        Scanner sc=new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {
            System.out.println("\n-------------------------------------------------------");
            System.out.println("              Bienvenido al Modulo 2.0                  ");
            System.out.println("-------------------------------------------------------");
            System.out.println("¿Qué acción desea realizar?");
            System.out.println("1. Consultar categoría de lealtad de cliente");
            System.out.println("2. Consultar lista de clientes por categoría de lealtad");
            System.out.println("3. Salir");

            System.out.println("Escribe una de las opciones:");
            opcion = sc.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("\n.............................");
                    System.out.println("Has seleccionado la opcion 1");
                    System.out.println(".............................\n");
                    break;
                case 2:
                    System.out.println("\n.............................");
                    System.out.println("Has seleccionado la opcion 2");
                    System.out.println(".............................\n");
                    break;
                case 3:
                    salir = true;
                    break;
                default:
                    System.out.println("Solo números entre 1 y 3");
            }

            if (opcion == 1) {

                System.out.println("Ingresar RUC a Buscar: ");
                long RUC1;
                RUC1 = sc.nextLong();


                String consulta = consultaCliente(RUC1);
                System.out.println("El resultado del RUC: " + RUC1 + "\n" + consulta);
            }

            else if (opcion==2){


            sc.nextLine();
            System.out.println("Ingrese Categoria de Lealtad: ");
            String categLealtad;
            categLealtad=sc.nextLine();


            System.out.println("Ingrese Sucursal: ");
            String sucursal;
            sucursal=sc.nextLine();

            System.out.println("--------------------------------------------------------------------------------------");
            System.out.println("Segun la Categoria de Lealtad " + "[ " + categLealtad +" ]" + " y Sucursal " +  "[ " +  sucursal + " ]"+ " los clientes son: ");
            String[] listaClientes=clientesLealtad(categLealtad,sucursal);

            } else {
                System.out.println("\n.............................");
                System.out.println("              Gracias          ");
            }

        }
    }

    private static String consultaCliente(Long RUC1){
        String datosCliente="";

        for (int i=0; i<RUC.length;i++){
            if(RUC1.equals(RUC[i])){
                datosCliente ="- Sucursal de atencion: "+ sucursales[i]+ "\n- Razon Social: "+ razonSocial[i] + "\n- Contacto: "+ personaDeContacto[i] +
                        "\n- Satisfacción: "+ satisfaGeneral[i] + "\n- Recompra: "+ intenRecompra[i]+ "\n- Recomendación: "+ intenRecomendacion[i]+
                        "\n- Categoria de Lealtad: "+ categoriaLealtad[i] + "\n- Comentario: "+ sugerencia[i];}
            }
        return datosCliente;
    }

    private static String [] clientesLealtad(String categLealtad, String sucursal) {
        String[] clientes = new String[sucursales.length];
        int indice=0;

        for (int i = 0; i <categoriaLealtad.length; i++)
            if (categoriaLealtad[i].equals(categLealtad) && sucursales[i].equals(sucursal)) {
                clientes[indice] = ("\n- Razon Social: " + razonSocial[i] +"\n- Contacto: "+ personaDeContacto[i] +
                        "\n- La Categoria de lealtad es: "+ categoriaLealtad[i]);
                System.out.println("\nCliente " + i + ": " + clientes[indice]);
                indice += 1;

            }
        return clientes;
    }
}
