package INICIO;
import java.util.Scanner;

public class ModuloClienteExterno {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String sucursal;
        long RUC;
        String nombresCompletos;
        String sugerencia;

        int indiceSatisfacion;
        int indiceRecompra;
        int indiceRecomendacion;
        boolean salir= false;
        int opcion;
        String nombre;

        while (!salir) {
            System.out.println("\n--------------------------");
            System.out.println("Bienvenido a la plataforma");
            System.out.println("--------------------------");
            System.out.println("- Ingrese su nombre: ");
            nombre= sc.nextLine();

            System.out.println("\n-----------------------------");
            System.out.println("       Bienvenido " + nombre);
            System.out.println("-----------------------------");

            System.out.println("Desea realizar una encuesta: ");
            System.out.println("1. Si");
            System.out.println("2. No");

            System.out.println("Ingrese opción: ");
            opcion = sc.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("Continue con la encuesta");
                    System.out.println(".........................\n");
                    break;
                case 2:
                    salir = true;
                    break;
            }

            if (opcion==1){

                sc.nextLine();

                System.out.println("- Ingresar la sucursal que visito: ");
                sucursal = sc.nextLine();

                System.out.println("- Ingresar RUC: ");
                RUC = sc.nextLong();

                sc.nextLine();
                System.out.println("- Ingresar nombres completos: ");
                nombresCompletos = sc.nextLine();

                System.out.println("\n Por favor, califique las preguntas de acuerdo a la siguiente escala:");
                System.out.println(" - Del 9 al 10 - Satisfecho/Probable");
                System.out.println(" - Del 6 al 8 - Parcialmente Satisfecho/Parcialmente Probable");
                System.out.println(" - Del 1 al 5 - Insatisfecho/No es Probable");
                System.out.println("-------------------------------------------------------------------------------------");

                System.out.println("¿Que tan satisfecho esta con la atención del dia de hoy en la tienda de " + sucursal + " ?: ");
                indiceSatisfacion = sc.nextInt();

                System.out.println("¿Que tan probable es que vuelva a comprar repuestos en la tienda de " + sucursal + "?: ");
                indiceRecompra = sc.nextInt();

                System.out.println("¿Que tan probable es que recomiende a algun colega comprar repuestos en la tienda de " + sucursal + "?: ");
                indiceRecomendacion = sc.nextInt();

                sc.nextLine();
                System.out.println("¿Desea dejar una sugerencia para mejorar nuestro servicio?");
                sugerencia = sc.nextLine();

                String categoria = calcularCategoriaLealtad(indiceSatisfacion, indiceRecompra, indiceRecomendacion);
                System.out.println("\n.........................................");
                System.out.println("Su categoria de lealtad es: " + categoria);
                System.out.println(".........................................");

                String cierre = mensajeFinal(indiceSatisfacion,indiceRecompra,indiceRecomendacion);
                System.out.println("\n" + cierre);

                System.out.println("\n.............................................................................");
                System.out.println("El dia de hoy " + nombresCompletos + " con RUC " + RUC + " visito la sucursal de \n" + sucursal );
                System.out.println("Sugerencia: "+ sugerencia);
                System.out.println(".............................................................................");

            }else {
                System.out.println("\n.............................");
                System.out.println("            Gracias          ");
            }
        }
    }

    static String calcularCategoriaLealtad(int indiceSatisfaccion, int indiceRecompre, int indiceRecomendacion) {
        String categoria;

        if (indiceSatisfaccion >= 9 && indiceRecompre >= 9 && indiceRecomendacion >= 9) {
            categoria = "LEAL";
        } else if (indiceSatisfaccion <= 5 || indiceRecompre <= 5 || indiceRecomendacion <= 5) {
            categoria = "RIESGO";
        } else {
            categoria = "VULNERABLE";
        }
        return categoria;

    }

    static String mensajeFinal(int indiceSatisfaccion, int indiceRecompre, int indiceRecomendacion) {
        String categoria = calcularCategoriaLealtad(indiceSatisfaccion, indiceRecompre, indiceRecomendacion);
        String mensajeFinal= "";

        if ("LEAL".equals(categoria)) {
            mensajeFinal = "Gracias por su visita";
        } else if ("RIESGO".equals(categoria) || "VULNERABLE".equals(categoria)) {
            mensajeFinal = "Sentimos los inconvenientes del dia de hoy, en los siguientes dias un representante de Ferreyros se estara comunicando con usted. \nMuchas gracias por su visita.";
        }
        return mensajeFinal;
    }
}

